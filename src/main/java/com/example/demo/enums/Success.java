package com.example.demo.enums;

public enum Success {
    DEFAULT("success.default");

    Success(String message) {
        this.message = message;
    }
    private String message;

    public String getMessage() {
        return message;
    }

}
