package com.example.demo.repository;
import com.example.demo.entity.Note;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface NotesRepository extends JpaRepository<Note, Long>{
}
