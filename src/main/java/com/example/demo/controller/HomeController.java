package com.example.demo.controller;

import com.example.demo.dto.NoteDto;
import com.example.demo.service.noteService.NoteService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

@Controller
public class HomeController {

    private final NoteService noteService;
    public HomeController(NoteService noteService) {
        this.noteService = noteService;
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PostMapping(value = "/saveNote")
    public ResponseEntity<?> postNotes(@RequestBody NoteDto request) {
        return ResponseEntity.ok(noteService.postNotes(request));
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping(value = "/allNotes")
    public ResponseEntity<?> getAllNotes() {
        return ResponseEntity.ok(noteService.getAllNotes());
    }
}
