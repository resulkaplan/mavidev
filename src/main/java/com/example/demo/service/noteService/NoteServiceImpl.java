package com.example.demo.service.noteService;

import com.example.demo.dto.BaseResponse;
import com.example.demo.dto.GenericResponse;
import com.example.demo.dto.NoteDto;
import com.example.demo.entity.Note;
import com.example.demo.enums.Success;
import com.example.demo.repository.NotesRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class NoteServiceImpl implements  NoteService{

    private final NotesRepository notesRepository;
    public NoteServiceImpl( NotesRepository notesRepository)
    {
        this.notesRepository = notesRepository;
    }

    @Override
    public BaseResponse postNotes(NoteDto request){
        Note notes = new Note();
        notes.setNote(request.getNote());
        notesRepository.save(notes);
        return new GenericResponse<>(Success.DEFAULT.getMessage());
    }

    @Override
    public BaseResponse getAllNotes(){
        List<Note> allNotes = notesRepository.findAll();
        return new GenericResponse<>(Success.DEFAULT.getMessage(),allNotes);
    }
}
