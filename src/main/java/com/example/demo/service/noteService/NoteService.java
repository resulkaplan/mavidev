package com.example.demo.service.noteService;

import com.example.demo.dto.BaseResponse;
import com.example.demo.dto.NoteDto;

public interface NoteService {
     BaseResponse postNotes(NoteDto request);
     BaseResponse getAllNotes();
}
